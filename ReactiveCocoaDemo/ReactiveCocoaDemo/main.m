//
//  main.m
//  ReactiveCocoaDemo
//
//  Created by w on 16/8/30.
//  Copyright © 2016年 lhwhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
