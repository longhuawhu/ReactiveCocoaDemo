//
//  ViewController.m
//  ReactiveCocoaDemo
//
//  Created by w on 16/8/30.
//  Copyright © 2016年 lhwhu. All rights reserved.
//

#import "ViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    [self demo01];
//    [self demoForSideEffects];
 //   [self demoForMulticast];
 //   [self demoForTransformingStreams];
  //  [self demoForCombiningStreams];
//    [self demoForFlatten];
    
//    [self demoForThen];
//    [self demoMerging];
//    [self demoForCcombineLatest];
    [self demoForSwitchToLatest];
}

- (void)demo01{
    RACSignal *letters = [@"A B C D E F G H I" componentsSeparatedByString:@" "].rac_sequence.signal;
    
    [letters subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
}

// For a cold signal, side effects will be performed once per subscription:
- (void)demoForSideEffects{
    
    __block unsigned subscriptions = 0;
    
    RACSignal *loggingSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        subscriptions++;
        [subscriber sendCompleted];
        return nil;
    }];
    
    [loggingSignal subscribeCompleted:^{
        NSLog(@"subscriptions %u", subscriptions);
    }];
    
    [loggingSignal subscribeCompleted:^{
        NSLog(@"subscriptions %u", subscriptions);
    }];
}

- (void)demoForMulticast{
    __block unsigned subscriptions = 0;
    
    RACSignal *loggingSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        subscriptions++;
        [subscriber sendCompleted];
        return nil;
    }];
    
    RACMulticastConnection *connection = [loggingSignal multicast:[RACReplaySubject subject]];
    
    [connection connect];
    
    [connection.signal subscribeCompleted:^{
        NSLog(@"subscriptions %u", subscriptions);
    }];
    
    [connection.signal subscribeCompleted:^{
        NSLog(@"subscriptions %u", subscriptions);
    }];
}

- (void)demoForTransformingStreams{
    RACSequence *letters = [@"A B C D E F G H I" componentsSeparatedByString:@" "].rac_sequence;
    
    RACSequence *mapped = [letters map:^id(id value) {
        return [value stringByAppendingString:@"*"];
    }];
    
    [mapped.signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    
    RACSequence *numbers = [@"1 2 3 4 5 6 7" componentsSeparatedByString:@" "].rac_sequence;
    
    [[numbers filter:^BOOL(NSString *value) {
        return (value.intValue % 2) == 0;
    }].signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];

}

- (void)demoForCombiningStreams{
    
    //These operators combine multiple streams into a single new stream.
    RACSequence *letters = [@"A B C D E F G H I" componentsSeparatedByString:@" "].rac_sequence;
    RACSequence *numbers = [@"1 2 3 4 5 6 7 8 9" componentsSeparatedByString:@" "].rac_sequence;
    
    // Contains: A B C D E F G H I 1 2 3 4 5 6 7 8 9
    RACSequence *concatenated = [letters concat:numbers];
    
    [concatenated.signal subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    
    //the -flatten operator is applied to a stream-of-streams, and combines their values into a single new stream.
    RACSequence *sequenceOfSequences = @[ letters, numbers ].rac_sequence;
    
    RACSequence *flattened = [sequenceOfSequences flatten];
}

- (void)demoForFlatten{
    RACSubject *letters = [RACSubject subject];
    RACSubject *numbers = [RACSubject subject];
    RACSignal *signalOfSignals = [RACSignal createSignal:^ RACDisposable * (id<RACSubscriber> subscriber) {
        [subscriber sendNext:letters];
        [subscriber sendNext:numbers];
        [subscriber sendCompleted];
        return nil;
    }];
    
    RACSignal *flattened = [signalOfSignals flatten];
    
    // Outputs: A 1 B C 2
    [flattened subscribeNext:^(NSString *x) {
        NSLog(@"%@", x);
    }];
    
    [letters sendNext:@"A"];
    [numbers sendNext:@"1"];
    [letters sendNext:@"B"];
    [letters sendNext:@"C"];
    [numbers sendNext:@"2"];
}

- (void)demoForThen{
    RACSignal *letters = [@"A B C D E F G H I" componentsSeparatedByString:@" "].rac_sequence.signal;
    
    // The new signal only contains: 1 2 3 4 5 6 7 8 9
    //
    // But when subscribed to, it also outputs: A B C D E F G H I
    RACSignal *sequenced = [[letters
                             doNext:^(NSString *letter) {
                                 NSLog(@"%@", letter);
                             }]
                            then:^{
                                return [@"1 2 3 4 5 6 7 8 9" componentsSeparatedByString:@" "].rac_sequence.signal;
                            }];
    
    [sequenced subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    
}
//The +merge: method will forward the values from many signals into a single stream, as soon as those values arrive:
- (void)demoMerging{
    RACSubject *letters = [RACSubject subject];
    RACSubject *numbers = [RACSubject subject];
    RACSignal *merged = [RACSignal merge:@[ letters, numbers ]];
    
    // Outputs: A 1 B C 2
    [merged subscribeNext:^(NSString *x) {
        NSLog(@"%@", x);
    }];
    
    [letters sendNext:@"A"];
    [numbers sendNext:@"1"];
    [letters sendNext:@"B"];
    [letters sendNext:@"C"];
    [numbers sendNext:@"2"];
    
}

- (void)demoForCcombineLatest{
    RACSubject *letters = [RACSubject subject];
    RACSubject *numbers = [RACSubject subject];
    RACSignal *combined = [RACSignal
                           combineLatest:@[letters, numbers]
                           reduce:^(NSString *letter, NSString *number) {
                               return [letter stringByAppendingString:number];
                           }];
    
    // Outputs: B1 B2 C2 C3
    [combined subscribeNext:^(id x) {
        NSLog(@"%@", x);
    }];
    
    [letters sendNext:@"A"];
    [letters sendNext:@"B"];
    [numbers sendNext:@"1"];
    [numbers sendNext:@"2"];
    [letters sendNext:@"C"];
    [numbers sendNext:@"3"];

}

//The -switchToLatest operator is applied to a signal-of-signals, and always forwards the values from the latest signal:
- (void)demoForSwitchToLatest{
    RACSubject *letters = [RACSubject subject];
    RACSubject *numbers = [RACSubject subject];
    RACSubject *signalOfSignals = [RACSubject subject];
    
    RACSignal *switched = [signalOfSignals switchToLatest];
    
    // Outputs: A B 1 D
    [switched subscribeNext:^(NSString *x) {
        NSLog(@"%@", x);
    }];
    
    [signalOfSignals sendNext:letters];
    [letters sendNext:@"A"];
    [letters sendNext:@"B"];
    
    [signalOfSignals sendNext:numbers];
    [letters sendNext:@"C"];
    [numbers sendNext:@"1"];
    
    [signalOfSignals sendNext:letters];
    [numbers sendNext:@"2"];
    [letters sendNext:@"D"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
